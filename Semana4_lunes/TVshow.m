//
//  tvshows.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "TVshow.h"

@implementation TVshow


#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self) {
        _id = [aDecoder decodeObjectForKey:@"id"];
        _title = [aDecoder decodeObjectForKey:@"title"];
        _description = [aDecoder decodeObjectForKey:@"description"];
        _rating = [aDecoder decodeIntegerForKey:@"rating"];
    }
    return self;
}




- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.id forKey:@"id"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.description forKey:@"description"];
    [aCoder encodeInteger:self.rating forKey:@"rating"];
}


#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone{
    TVshow * showCopy = [[[self class] allocWithZone:zone] init];
    if(showCopy){
        //Objects
        showCopy.id = [self.id copyWithZone:zone];
        showCopy.title = [self.title copyWithZone:zone];
        showCopy.description = [self.description copyWithZone:zone];
        
        //Scalars
        showCopy.rating = self.rating;
    }
    return showCopy;
}

#pragma mark - Compare

- (BOOL)isEqualToTVshow:(TVshow *)show{
    if(![self.title isEqualToString:show.title]){
        return NO;
    }
    return YES;
}

- (BOOL)isEqual:(TVshow *)show{
    if(self == show){
        return YES;
    }
    if(![show isKindOfClass:[self class]]){
        return NO;
    }
    return [self isEqualToTVshow:show];
}

- (NSUInteger)hash{
    return [_title hash];
}





@end
