//
//  movies.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "Movie.h"

@implementation Movie

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self) {
        _id = [aDecoder decodeObjectForKey:@"id"];
        _title = [aDecoder decodeObjectForKey:@"title"];
        _description = [aDecoder decodeObjectForKey:@"description"];
        _rating = [aDecoder decodeIntegerForKey:@"rating"];
    }
    return self;
}


- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.id forKey:@"id"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.description forKey:@"description"];
    [aCoder encodeInteger:self.rating forKey:@"rating"];
}



#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone{
    Movie * movieCopy = [[[self class] allocWithZone:zone] init];
    if(movieCopy){
        //Objects
        movieCopy.id = [self.id copyWithZone:zone];
        movieCopy.title = [self.title copyWithZone:zone];
        movieCopy.description = [self.description copyWithZone:zone];
        
        //Scalars
        movieCopy.rating = self.rating;
    }
    return movieCopy;
}

@end
