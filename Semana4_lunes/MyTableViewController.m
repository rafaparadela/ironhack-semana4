//
//  MyTableViewController.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "MyTableViewController.h"
#import "MyViewCell.h"
#import "TVshow.h"

static NSString * const savedShowsFileName=@"shows";

@interface MyTableViewController ()

@property (nonatomic, strong) NSMutableArray * myshows;

@end

@implementation MyTableViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.title = @"Series";
//        _myshows = [NSMutableArray array];
    }
    return self;
}

- (NSMutableArray *)myshows
{
    if (!_myshows) {
        _myshows = [NSMutableArray array];
    }
    return _myshows;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self load];
//    self.title = @"Series";
//    _myshows = [NSMutableArray array];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Añadir elementos
- (IBAction)addRandom:(id)sender {
    [self.myshows addObject:[self randomShow]];
    
    [self.tableView reloadData];

}

- (IBAction)copyLast:(id)sender {
    
    if([self.myshows count] > 0){
        TVshow * copyshow = [[self.myshows lastObject] copy];
        [self.myshows addObject:copyshow];
        [self.tableView reloadData];
    }
    
    [self save];
    
}

- (TVshow *) randomShow{
    NSArray * series = @[@"Prison Break",@"Lost",@"The Sopranos",@"The Wire",@"Breaking Bad",@"House of Cards",@"True Detective",@"game of Thrones"];
    NSUInteger randomIndex = arc4random() % [series count];
    TVshow * show = [[TVshow alloc] init];
    show.title = [series objectAtIndex:randomIndex];
    show.description = @"Description caramba";
    return show;
}

- (NSString *) archivePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    return  [documentsDirectory stringByAppendingPathComponent:savedShowsFileName];
    
}

- (void) save{
    
    if (self.myshows.count) {
        [NSKeyedArchiver archiveRootObject:self.myshows toFile:[self archivePath] ];
    }
}

- (void) load{
    NSArray *saved = [NSKeyedUnarchiver unarchiveObjectWithFile:[self archivePath]];
//
    if(saved.count){
        self.myshows = [saved mutableCopy];
    }
    
}




- (IBAction)compare:(id)sender {
    
    BOOL comp = NO;
    if([self.myshows count]>0){
        TVshow * first = [self.myshows firstObject];
        TVshow * last = [self.myshows lastObject];
        
        if([first isEqual: last]){
            comp = YES;
        }
    }
    
    
    NSLog(@"%d",comp);
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.myshows count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell" forIndexPath:indexPath];
    
    TVshow * serie = [self.myshows objectAtIndex:indexPath.row];
    cell.myTitle.text = serie.title;
    cell.myDescription.text = serie.description;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
